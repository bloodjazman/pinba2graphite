#!/usr/bin/env python
# coding=utf-8
from ConfigParser import ConfigParser
import glob
import os
import re
import argparse
import pymysql
import datetime
import time
import socket
import pickle
import struct
import logging

def load_config(cfg_names):
    config = ConfigParser()
    for f in glob.glob(cfg_names):
        if os.path.isfile(f):
            config.read(f)
            logging.debug('Load config from %s' % f)
    return config


clean_name = re.compile('[^0-9a-zA-Z]+')
clean_symbols = re.compile('_+')


def clearify(name):
    if name is None:
        return 'any'
    return clean_symbols.sub('_', clean_name.sub('_', name.replace('.', '_')))


def round_time(dt=None, roundTo=60):
    """Round a datetime object to any time laps in seconds
    dt : datetime.datetime object, default now.
    roundTo : Closest number of seconds to round to, default 1 minute.
    Author: Thierry Husson 2012 - Use it as you want but don't blame me.
    """
    if dt == None: dt = datetime.datetime.now()
    seconds = (dt - dt.min).seconds
    # // is a floor division, not a comment on following line:
    rounding = (seconds + roundTo / 2) // roundTo * roundTo
    return dt + datetime.timedelta(0, rounding - seconds, -dt.microsecond)


def load_from_pinba(config):
    con = pymysql.connect(
        host=config.get('pinba', 'host'), port=config.getint('pinba', 'port'),
        user=config.get('pinba', 'user'), password=config.get('pinba', 'password'),
        database=config.get('pinba', 'database')
    )
    q = con.cursor(pymysql.cursors.DictCursor)
    now = float('%d' % time.mktime(round_time(roundTo=config.getint('pinba', 'pinba_stats_history')).timetuple()))
    # traffic and average metrics by host, scripts and requests
    dimensions = ('hostname', 'server_name', 'script_name')
    sql = """
        SELECT
            hostname, server_name, script_name,
            req_count, req_per_sec, req_time_total, req_time_percent, req_time_per_sec, req_time_median,
            ru_utime_total, ru_utime_percent, ru_utime_per_sec,
            ru_stime_total, ru_stime_percent, ru_stime_per_sec,
            traffic_total, traffic_percent, traffic_per_sec,
            memory_footprint_total, memory_footprint_percent
         FROM report_by_hostname_server_and_script
    """
    for metrica in iterate_metrics(q, sql, dimensions, now, config.get('graphite','prefix')):
        yield metrica

    # traffic and histogram script metrics by host, scripts and requests
    dimensions = ('hostname', 'server_name', 'script_name')
    sql = """
        SELECT
            r.hostname,
            r.server_name,
            r.script_name,
            /* COUNT(DISTINCT r.id) AS req_count, */

            COUNT(DISTINCT IF(r.status BETWEEN 400 AND 550, r.id, NULL)) AS error_count,
            100 * COUNT(DISTINCT IF(r.status BETWEEN 400 AND 550, r.id, NULL)) / COUNT(DISTINCT r.id) AS error_percent,

            COUNT(DISTINCT IF(r.req_time < 0.2, r.id, NULL)) AS req_less_02s,
            COUNT(DISTINCT IF(r.req_time >= 0.2 AND r.req_time < 0.5, r.id, NULL)) AS req_02s_05s,
            COUNT(DISTINCT IF(r.req_time >= 0.5 AND r.req_time < 1, r.id, NULL)) AS req_05s_1s,
            COUNT(DISTINCT IF(r.req_time >= 1 AND r.req_time < 2, r.id, NULL)) AS req_1s_2s,
            COUNT(DISTINCT IF(r.req_time >= 2, r.id, NULL)) AS req_more_2s,

            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 50, r.req_time, NULL ) ) AS req_50_percentile,
            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 90, r.req_time, NULL ) ) AS req_90_percentile,
            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 95, r.req_time, NULL ) ) AS req_95_percentile,
            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 98, r.req_time, NULL ) ) AS req_98_percentile
        FROM (
             SELECT
                r.id, r.req_time, r.script_name, r.server_name, r.hostname, r.status, rc.r_total,
                @rank := CASE
                    WHEN @script_name <> r.script_name OR @server_name<> r.server_name OR @hostname <> r.hostname
                    THEN CONCAT(LEFT(@hostname:=r.hostname, 0), LEFT(@server_name:=r.server_name, 0), LEFT(@server_name:=r.server_name, 0), 0)
                    ELSE @rank+1
                END AS rank,
                @script_name := r.script_name, @server_name := r.server_name, @hostname :=  r.hostname
             FROM (SELECT * FROM request ORDER BY script_name, server_name, hostname, req_time DESC) AS r
             INNER JOIN (SELECT @hostname := -1) AS h
             INNER JOIN (SELECT @script_name := -1) AS s
             INNER JOIN (SELECT @server_name := -1) AS srv
             INNER JOIN (SELECT COUNT(id) AS r_total, script_name, server_name, hostname FROM request GROUP BY script_name, server_name, hostname) AS rc
             ON rc.script_name=r.script_name AND rc.hostname=r.hostname AND rc.server_name=r.server_name

        ) AS r
        GROUP BY r.hostname, r.server_name, r.script_name
    """
    for metrica in iterate_metrics(q, sql, dimensions, now, config.get('graphite','prefix')):
        yield metrica

    # histrogram and percentile by tags and requests
    dimensions = ('hostname', 'server_name', 'script_name', 'tag_name', 'tag_value')
    sql = """
        SELECT
            r.hostname,
            r.server_name,
            r.script_name,
            t.tag_name,
            t.tag_value,
            COUNT(DISTINCT t.id) AS tag_count,

            COUNT(DISTINCT IF(r.status BETWEEN 400 AND 550, t.id, NULL)) AS tag_error_count,
            100 * COUNT(DISTINCT IF(r.status BETWEEN 400 AND 550, t.id, NULL)) / COUNT(DISTINCT t.id) AS tag_error_percent,

            AVG(t.value) AS tag_by_req_avg ,
            SUM(t.value)/SUM(r.req_time) AS tag_req_percent,

            COUNT(DISTINCT IF(t.value < 0.1, t.id, NULL)) AS tag_less_01s,
            COUNT(DISTINCT IF(t.value >= 0.1 AND t.value < 0.2, t.id, NULL)) AS tag_01s_02s,
            COUNT(DISTINCT IF(t.value >= 0.2 AND t.value < 0.5, t.id, NULL)) AS tag_02s_05s,
            COUNT(DISTINCT IF(t.value >= 0.5 AND t.value < 1.0, t.id, NULL)) AS tag_05s_1s,
            COUNT(DISTINCT IF(t.value >= 1.0, t.id, NULL)) AS tag_more_1s,

            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 50, r.req_time, NULL ) ) AS req_50_percentile,
            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 90, r.req_time, NULL ) ) AS req_90_percentile,
            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 95, r.req_time, NULL ) ) AS req_95_percentile,
            MIN(IF(ROUND(100*(r.r_total - r.rank) / r.r_total, 2) >= 98, r.req_time, NULL ) ) AS req_98_percentile,

            MIN(IF(ROUND(100*(t.t_total - t.rank) / r.r_total, 2) >= 50, t.value, NULL ) ) AS tag_50_percentile,
            MIN(IF(ROUND(100*(t.t_total - t.rank) / r.r_total, 2) >= 90, t.value, NULL ) ) AS tag_90_percentile,
            MIN(IF(ROUND(100*(t.t_total - t.rank) / r.r_total, 2) >= 95, t.value, NULL ) ) AS tag_95_percentile,
            MIN(IF(ROUND(100*(t.t_total - t.rank) / t.t_total, 2) >= 98, t.value, NULL ) ) AS tag_98_percentile

        FROM (
             SELECT
                r.id, r.req_time, r.script_name, r.server_name, r.hostname, r.status, rc.r_total,
                @rank := CASE
                    WHEN @script_name <> r.script_name OR @server_name<> r.server_name OR @hostname <> r.hostname
                    THEN CONCAT(LEFT(@hostname:=r.hostname, 0), LEFT(@server_name:=r.server_name, 0), LEFT(@server_name:=r.server_name, 0), 0)
                    ELSE @rank+1
                END AS rank,
                @script_name := r.script_name, @server_name := r.server_name, @hostname :=  r.hostname
             FROM (SELECT * FROM request ORDER BY script_name, server_name, hostname, req_time DESC) AS r
             INNER JOIN (SELECT @hostname := -1) AS h
             INNER JOIN (SELECT @script_name := -1) AS s
             INNER JOIN (SELECT @server_name := -1) AS srv
             INNER JOIN (SELECT COUNT(id) AS r_total, script_name, server_name, hostname FROM request GROUP BY script_name, server_name, hostname) AS rc
             ON rc.script_name=r.script_name AND rc.hostname=r.hostname AND rc.server_name=r.server_name

        ) AS r
        INNER JOIN (
            SELECT
                tt.id, tt.request_id, tt.tag_name, tt.tag_value, tt.value, ttc.t_total,
                @rank := CASE
                    WHEN @script_name <> tt.script_name OR @server_name<> tt.server_name OR @hostname <> tt.hostname OR @tag_name <> tt.tag_name OR @tag_value <> tt.tag_value
                    THEN CONCAT(LEFT(@hostname:=tt.hostname, 0), LEFT(@server_name:=tt.server_name, 0), LEFT(@server_name:=tt.server_name, 0), LEFT(@tag_name:=tt.tag_name, 0), LEFT(@tag_value:=tt.tag_value, 0), 0)
                    ELSE @rank+1
                END AS rank,
                @script_name := tt.script_name, @server_name := tt.server_name, @hostname := tt.hostname, @tag_name := tt.tag_name, @tag_value := tt.tag_value
            FROM (
                SELECT
                    t.request_id, t.id, tg.name AS tag_name, ttg.value AS tag_value, t.value,
                    r.hostname, r.script_name, r.server_name
                FROM timer AS t
                INNER JOIN request AS r
                ON t.request_id=r.id
                INNER JOIN timertag AS ttg
                ON ttg.timer_id=t.id
                INNER JOIN tag AS tg
                ON tg.id=ttg.tag_id
                ORDER BY r.script_name, r.server_name, r.hostname, tg.name, ttg.value, t.value DESC
            ) AS tt
            INNER JOIN (SELECT @tag_name := -1) AS tn
            INNER JOIN (SELECT @tag_value := -1) AS tv
            INNER JOIN (SELECT @hostname := -1) AS h
            INNER JOIN (SELECT @script_name := -1) AS s
            INNER JOIN (SELECT @server_name := -1) AS srv
            INNER JOIN (
                SELECT
                    COUNT(DISTINCT t.id) AS t_total, tg.name AS tag_name, ttg.value AS tag_value,
                    r.hostname, r.script_name, r.server_name
                FROM timer AS t
                INNER JOIN request AS r
                ON t.request_id=r.id
                INNER JOIN timertag AS ttg
                ON ttg.timer_id=t.id
                INNER JOIN tag AS tg
                ON tg.id=ttg.tag_id
                GROUP BY r.script_name, r.server_name, r.hostname, tg.name, ttg.value
            ) AS ttc
            ON ttc.script_name = tt.script_name AND ttc.server_name = tt.server_name AND ttc.hostname = tt.hostname
            AND ttc.tag_name=tt.tag_name AND ttc.tag_value=tt.tag_value

        ) AS t
        ON t.request_id=r.id
        GROUP BY r.hostname, r.server_name, r.script_name, t.tag_name, t.tag_value
    """
    for metrica in iterate_metrics(q, sql, dimensions, now, config.get('graphite','prefix')):
        yield metrica

    con.close()


def iterate_metrics(q, sql, dimensions, now, prefix):
    q.execute('SET @rank := 0')
    q.execute(sql)
    for row in q:
        name = ''
        for d in dimensions:
            name += clearify(row[d]) + '.'
        name = prefix + '.' + name[:-1]
        for metrica_name in row:
            if metrica_name not in dimensions:
                value = row[metrica_name]
                if value is None:
                    value = 0
                yield (name + '.' + metrica_name, (str(value), int(now)))


def connect_to_graphite(config):
    connection = socket.socket()
    plain_protocol = config.has_option('graphite', 'plain_listener')

    if plain_protocol:
        host, port = config.get('graphite', 'plain_listener').split(':')
    else:
        host, port = config.get('graphite', 'pickle_listener').split(':')
    if port is None:
        port = 2004

    try:
        connection.connect((host, int(port)))
        logging.debug('Connected to %s:%s plain_protocol=%s' % (host, port, plain_protocol))
    except socket.error:
        raise SystemExit("Couldn't connect to %s:%s, is carbon-cache.py running?" % (host, port))
    return connection


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Script for aggregation PINBA metrics to GRAPHITE')
    parser.add_argument(
        '--config', metavar='FILENAME', default='/etc/pinba2graphite.conf',
        required=True, type=str, help='path to configuration file',
    )
    parser.add_argument(
        '--verbose', default=False, required=False, type=bool, help='verbose mode',
    )

    args = parser.parse_args()
    if args.verbose:
        logging.basicConfig(level = logging.DEBUG)

    cfg = load_config(args.config)
    sock = connect_to_graphite(cfg)

    lines = []
    tuples = ([])
    sended = 0
    plain_protocol = cfg.has_option('graphite', 'plain_listener')
    chunk_size = cfg.getint('graphite', 'chunk_size')

    for metrica in load_from_pinba(cfg):
        logging.debug(metrica)

        if plain_protocol:
            lines.append(' '.join( [
                metrica[0], str(metrica[1][0]), str(metrica[1][1])
            ] ))
            if len(lines) >= chunk_size:
                sock.sendall("\n".join(lines)+"\n")
                sended += chunk_size
                lines = []
        else:
            tuples.append(metrica)
            if len(tuples) >= chunk_size:
                package = pickle.dumps(tuples, 1)
                size = struct.pack('!L', len(package))
                sock.sendall(size)
                sock.sendall(package)
                sended += chunk_size
                tuples = ([])

    if len(lines) > 0:
        sock.sendall("\n".join(lines)+"\n")
        sended += len(lines)

    if len(tuples) > 0:
        package = pickle.dumps(tuples)
        header = struct.pack('!L', len(package))
        sock.sendall(header)
        sock.sendall(package)
        sended += len(tuples)

    logging.debug('Flush %d metrics' % sended)
    sock.close()
